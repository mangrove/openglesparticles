//
//  RetainedEAGLLayer.m
//  Particles
//
//  Created by Emil Korngold | Mangrove on 7/11/13.
//  Copyright (c) 2013 Emil Korngold | Mangrove. All rights reserved.
//

#import "RetainedEAGLLayer.h"

@implementation RetainedEAGLLayer
- (void)setDrawableProperties:(NSDictionary *)drawableProperties {
    // Copy the dictionary and add/modify the retained property
    NSMutableDictionary *mutableDictionary = [[NSMutableDictionary alloc] initWithCapacity:drawableProperties.count + 1];
    [drawableProperties enumerateKeysAndObjectsUsingBlock:^(id key, id object, BOOL *stop) {
        // Copy all keys except the retained backing
        if (![key isKindOfClass:[NSString class]]
			|| ![(NSString *)key isEqualToString:kEAGLDrawablePropertyRetainedBacking])
            [mutableDictionary setObject:object forKey:key];
    }];
    // Add the retained backing setting
    [mutableDictionary setObject:@(YES) forKey:kEAGLDrawablePropertyRetainedBacking];
	
    // Continue
    [super setDrawableProperties:mutableDictionary];
}
@end
