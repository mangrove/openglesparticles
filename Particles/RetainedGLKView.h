//
//  RetainedGLKView.h
//  Particles
//
//  Created by Emil Korngold | Mangrove on 7/11/13.
//  Copyright (c) 2013 Emil Korngold | Mangrove. All rights reserved.
//

#import <GLKit/GLKit.h>

@interface RetainedGLKView : GLKView

@end
