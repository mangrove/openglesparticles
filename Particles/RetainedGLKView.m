//
//  RetainedGLKView.m
//  Particles
//
//  Created by Emil Korngold | Mangrove on 7/11/13.
//  Copyright (c) 2013 Emil Korngold | Mangrove. All rights reserved.
//

#import "RetainedGLKView.h"
#import "RetainedEAGLLayer.h"

@implementation RetainedGLKView
+ (Class)layerClass {
    return [RetainedEAGLLayer class];
}
@end