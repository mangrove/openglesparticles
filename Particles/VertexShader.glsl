//
//  VertexShader.glsl
//

attribute highp vec4 a_position;
attribute lowp vec4 a_color;

varying highp vec4 v_position;
varying lowp vec4 v_color;

void main()
{
	v_color = a_color;
    gl_PointSize = 2.0;
    gl_Position =  a_position;
}
