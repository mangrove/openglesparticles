//
//  ViewController.m
//  Particles
//
//  Created by Emil Korngold | Mangrove on 6/27/13.
//  Copyright (c) 2013 Emil Korngold | Mangrove. All rights reserved.
//

static const GLfloat squareVertices[] = {
	-1.0f, -1.0f, 1.0f,
	1.0f, -1.0f, 1.0f,
	-1.0f, 1.0f, 1.0f,
	1.0f, 1.0f, 1.0f
};
static const GLfloat squareColors[] = {
	0.2f, 0.0f, 0.2f, 0.05f,
	0.2f, 0.0f, 0.2f, 0.05f,
	0.2f, 0.0f, 0.2f, 0.05f,
	0.2f, 0.0f, 0.2f, 0.05f
};

#import <QuartzCore/QuartzCore.h>
#import <GLKit/GLKit.h>
#import "ViewController.h"
#import "RetainedGLKView.h"
#import "RetainedEAGLLayer.h"

@interface ViewController ()

@property (nonatomic) GLuint program;

@property (nonatomic) int nrOfParticles;
@property (nonatomic) int canvasW;
@property (nonatomic) int canvasH;

@property (nonatomic) GLfloat * calcVertices;
@property (nonatomic) GLfloat * calcColors;
@property (nonatomic) GLfloat * calcSpeeds;
@property (nonatomic) GLfloat * calcBearings;

@property (nonatomic) int doubleItterator;
@property (nonatomic) int trippleItterator;

@property (nonatomic, strong) EAGLContext *context;

@property (nonatomic) GLuint positionPointer;
@property (nonatomic) GLuint colorPointer;

@end

@implementation ViewController

@synthesize context = _context;

- (void)viewDidLoad {
	
    [super viewDidLoad];
    
    self.context = [[EAGLContext alloc] initWithAPI:kEAGLRenderingAPIOpenGLES2];
    
    if (!self.context) {
        NSLog(@"Failed to create ES context");
    }
    
    RetainedGLKView *view = (RetainedGLKView *)self.view;
    view.context = self.context;
	view.layer.opaque = YES;
	view.layer.contentsScale = 1;
	view.contentScaleFactor = 1;
	
	[self initialSetup];
	[self setupGL];
}

- (void) initialSetup {
	
	// view size
	self.canvasW = self.view.bounds.size.width;
	self.canvasH = self.view.bounds.size.height;
	
	// number of particles, itterators
	self.nrOfParticles = 48000;
	self.doubleItterator = 2 * self.nrOfParticles;
	self.trippleItterator = 3 * self.nrOfParticles;
	
	// create particle vertices
	_calcVertices = malloc(self.nrOfParticles * 2 * sizeof(GL_FLOAT));
	for(int i=0; i<self.doubleItterator; i+=2) {
		// x
		_calcVertices[i] = arc4random() % 2000 * 0.001 - 1.0;
		// y
		_calcVertices[i+1] = arc4random() % 2000 * 0.001 - 1.0;
	}
	
	// create particle colors
	self.calcColors = malloc(self.nrOfParticles * 3 * sizeof(GL_FLOAT));
	for(int i=0; i<self.trippleItterator; i+=3) {
		// r
		self.calcColors[i] = 0.6f;
		// g
		self.calcColors[i+1] = 0.2f;
		// b
		self.calcColors[i+2] = 0.4f + arc4random() % 50 * 0.01;
	}
	
	// create particle speeds and bearings
	self.calcSpeeds = malloc(self.nrOfParticles * 2 * sizeof(GL_FLOAT));
	self.calcBearings = malloc(self.nrOfParticles * 2 * sizeof(GL_FLOAT));
	for(int i=0; i<self.doubleItterator; i+=3) {
		// speeds
		self.calcSpeeds[i] = arc4random() % 50 * 0.001 - 0.027;
		self.calcSpeeds[i+1] = arc4random() % 50 * 0.001 - 0.027;
		// bearings
		self.calcBearings[i] = arc4random() % 50 * 0.001 - 0.027;
		self.calcBearings[i+1] = arc4random() % 50 * 0.001 - 0.027;
	}
	
	// touch
	UITapGestureRecognizer * tapRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapAction:)];
	[self.view addGestureRecognizer:tapRecognizer];
	
	// hide status bar
	if ([self respondsToSelector:@selector(setNeedsStatusBarAppearanceUpdate)]) {
		// iOS 7
		[self performSelector:@selector(setNeedsStatusBarAppearanceUpdate)];
	} else {
		// iOS 6
		[[UIApplication sharedApplication] setStatusBarHidden:YES withAnimation:UIStatusBarAnimationSlide];
	}
}

- (BOOL)prefersStatusBarHidden {
	
	return YES;
}

- (void) tapAction:(UITapGestureRecognizer*)sender {
	
	// normalize tapPoint
	CGPoint tapPoint = [sender locationInView:self.view];
	tapPoint.x = (tapPoint.x/self.canvasW) * 2 - 1;
	tapPoint.y = -(tapPoint.y/self.canvasH) * 2 + 1;
	
	// change particle bearing
	for(int i=0; i<self.doubleItterator; i+=2) {
		CGFloat bearing = [self pointPairToBearingDegrees:CGPointMake(_calcVertices[i], _calcVertices[i+1]) secondPoint:tapPoint];
		_calcBearings[i] = cos(bearing) * 0.04;
		_calcBearings[i+1]= sin(bearing) * 0.04;
	}
}

- (CGFloat) pointPairToBearingDegrees:(CGPoint)startingPoint secondPoint:(CGPoint) endingPoint {
	
	// introduce random behaviour
	endingPoint.x += arc4random() % 20 * 0.01 - 0.1;
	endingPoint.y += arc4random() % 20 * 0.01 - 0.1;
	// calculate and return bearing
    CGPoint originPoint = CGPointMake(endingPoint.x - startingPoint.x, endingPoint.y - startingPoint.y);
    GLfloat bearingRadians = atan2f(originPoint.y, originPoint.x);
    return bearingRadians;
}

- (void)setupGL {
	
	[EAGLContext setCurrentContext:self.context];
	
	// setup shader
	GLuint vertexShader = [self createShaderWithFile:@"VertexShader.glsl" type:GL_VERTEX_SHADER];
	GLuint fragmentShader = [self createShaderWithFile:@"FragmentShader.glsl" type:GL_FRAGMENT_SHADER];
	
	// create program
	_program = glCreateProgram();
	
	// bind shader to program
	glAttachShader(_program, vertexShader);
	glAttachShader(_program, fragmentShader);
	glLinkProgram(_program);
    GLint linked = 0;
    glGetProgramiv(_program, GL_LINK_STATUS, &linked);
    if (linked == 0) {
        glDeleteProgram(_program);
        return;
    }
	
	// disable stuff we don't need to improve performance
	glDisable(GL_DEPTH_TEST);
	glDisable(GL_ALPHA_TEST);
	glDisable(GL_STENCIL_TEST);
			
	// tell GL to use our program
	glUseProgram(_program);
	
	// create shader pointers for position and color
	_positionPointer = glGetAttribLocation(_program, "a_position");
	glEnableVertexAttribArray(_positionPointer);
	_colorPointer = glGetAttribLocation(_program, "a_color");
    glEnableVertexAttribArray(_colorPointer);
}

- (GLuint)createShaderWithFile:(NSString *)filename type:(GLenum)type {
	
	// create shader object of given type
    GLuint shader = glCreateShader(type);
    if (shader == 0) return 0;
    
	// load shader files
    NSString *path = [[[NSBundle mainBundle] resourcePath] stringByAppendingPathComponent:filename];
    NSString *shaderString = [NSString stringWithContentsOfFile:path encoding:NSUTF8StringEncoding error:nil];
    const GLchar *shaderSource = [shaderString cStringUsingEncoding:NSUTF8StringEncoding];
    
	// compile shader
    glShaderSource(shader, 1, &shaderSource, NULL);
    glCompileShader(shader);
    
	// check if compiling succeeded
    GLint success = 0;
    glGetShaderiv(shader, GL_COMPILE_STATUS, &success);
    
	// throw error if compiling didn't succeed
    if (success == 0) {
        char errorMsg[2048];
        glGetShaderInfoLog(shader, sizeof(errorMsg), NULL, errorMsg);
        NSString *errorString = [NSString stringWithCString:errorMsg encoding:NSUTF8StringEncoding];
        NSLog(@"Failed to compile %@: %@", filename, errorString);
        glDeleteShader(shader);
        return 0;
    }
    
	// compiling succeeded, return shader
    return shader;
}

/**
 * override drawInRect, this is our main render tick called each frame 
 * (at aproximately 30fps if we did things right)
 */
- (void) glkView:(GLKView *)view drawInRect:(CGRect)rect {
    
	[self transform];
	[self render];
}

/**
 * transform, change vertice properties
 */
- (void) transform {
	
	int i;
    for(i=0; i<self.doubleItterator; i+=2) {
		
		// bearing x and y
		GLfloat xb = _calcBearings[i];
		GLfloat yb = _calcBearings[i+1];
		
		// speed x and y
		GLfloat xs = _calcSpeeds[i];
		GLfloat ys = _calcSpeeds[i+1];
		
		// x
		GLfloat newX = _calcVertices[i] + xs;
		if(newX > 1.0) {
			newX = -1.0;
		}else if(newX < -1.0) {
			newX = 1.0;
		}
		_calcVertices[i] = newX;
		
		// y
		GLfloat newY = _calcVertices[i+1] + ys;
		if(newY > 1.0) {
			newY = -1.0;
		}else if(newY < -1.0) {
			newY = 1.0;
		}
		_calcVertices[i+1] = newY;
		
		// move towards bearing
		_calcSpeeds[i] = xs + (xb - xs) * 0.05;
		_calcSpeeds[i+1] = ys + (yb - ys) * 0.05;
	}
}

/**
 * render, clear screen and render vertices
 */
- (void) render {
	
	// clear screen with color
	//glClearColor(0.2f, 0.0f, 0.2f, 1.0f);
	//glClear(GL_COLOR_BUFFER_BIT);
	
	// render fullscreen quad with alpha to create a fake motion blur effect
	glEnable(GL_BLEND);
	glBlendFunc(GL_ZERO, GL_ONE_MINUS_SRC_ALPHA);
	glVertexAttribPointer(_colorPointer, 4, GL_FLOAT, GL_FALSE, 0, squareColors);
	glVertexAttribPointer(_positionPointer, 3, GL_FLOAT, GL_FALSE, 0, squareVertices);
	glVertexPointer(2, GL_FLOAT, 0, squareVertices);
    glDrawArrays(GL_TRIANGLE_STRIP, 0, 4);
	
	// render points
	glDisable(GL_BLEND);
	glVertexAttribPointer(_colorPointer, 3, GL_FLOAT, GL_FALSE, 0, _calcColors);
	glVertexAttribPointer(_positionPointer, 2, GL_FLOAT, GL_FALSE, 0, _calcVertices);
	glDrawArrays(GL_POINTS, 0, self.nrOfParticles);
}

@end
