//
//  RetainedEAGLLayer.h
//  Particles
//
//  Created by Emil Korngold | Mangrove on 7/11/13.
//  Copyright (c) 2013 Emil Korngold | Mangrove. All rights reserved.
//

#import <QuartzCore/QuartzCore.h>

@interface RetainedEAGLLayer : CAEAGLLayer

@end