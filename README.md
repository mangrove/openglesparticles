# README #

My first adventure into programming iOS OpenGL graphics. 16.000 particles rendered as point sprites, 2D physics, fake motion blur by drawing a large quad over the previous rendering with a bit of alpha. Interactivity: tap repeatedly to set bearing for all particles. Running at approximately 30fps.

Demo video: https://www.youtube.com/watch?v=FxT5g8odtmQ

Enjoy!

Emil Korngold
emil@mangrove.nl